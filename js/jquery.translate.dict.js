//dictionary

var dictionary = {
	MSG01: {
	  en: "This fields is required.",
	  pt: "Este campo é obrigatório."
	},
	MSG02: {
	  en: "At least one snack must be selected for the order.",
	  pt: "Ao menos um lanche deve ser selecionado para o pedido."
	},
	//index.html
	nome: {
	  en: "Name",
	  pt: "Nome"
	},
	endereco: {
	  en: "Address",
	  pt: "Endereço"
	},
	"Lista de Lanches": {
	  en: "List of Snacks",
	  pt: "Lista de Lanches"
	},
	"Carne e Queijo": {
	  en: "Meat and Cheese",
	  pt: "Lista de Lanches"
	},
	"Vegetariano": {
	  en: "Vegetarian",
	  pt: "Vegetariano"
	},
	"Tipo de Pão:": {
	  en: "Type of Bread:",
	  pt: "Tipo de Pão:"
	},
	"Escolha o pão": {
	  en: "Choose the bread",
	  pt: "Escolha o pão"
	},
	"12 grãos": {
	  en: "12 grains",
	  pt: "12 grãos"
	},
	"3 queijos": {
	  en: "3 cheeses",
	  pt: "3 queijos"
	},
	"Queijo:": {
	  en: "Cheese:",
	  pt: "Queijo:"
	},
	"Sem Queijo": {
	  en: "Without Cheese",
	  pt: "Sem Queijo"
	},
	"Cheedar": {
	  en: "Cheddar",
	  pt: "Cheddar"
	},
	"Mussarela": {
	  en: "Mozzarella",
	  pt: "Mussarela"
	},
	"Recheio:": {
	  en: "Filling:",
	  pt: "Recheio:"
	},
	"Sem Recheio": {
	  en: "Without Filling",
	  pt: "Sem Recheio"
	},
	"Frango": {
	  en: "Chicken",
	  pt: "Frango"
	},
	"Carne": {
	  en: "Meat",
	  pt: "Carne"
	},
	"Rosbife": {
	  en: "Roast Beef",
	  pt: "Rosbife"
	},
	"Presunto": {
	  en: "Ham",
	  pt: "Presunto"
	},
	"Saladas:": {
	  en: "Salad:",
	  pt: "Saladas:"
	},
	"Alface": {
	  en: "Lettuce",
	  pt: "Alface"
	},
	"Rúcula": {
	  en: "Arugula",
	  pt: "Rúcula"
	},
	"Acelga": {
	  en: "Chard",
	  pt: "Acelga"
	},
	"Cebola": {
	  en: "Onion",
	  pt: "Cebola"
	},
	"Cebola roxa": {
	  en: "Purple onion",
	  pt: "Cebola roxa"
	},
	"Molhos:": {
	  en: "Sauces:",
	  pt: "Molhos:"
	},
	"Cebola Agridoce": {
	  en: "Bittersweet Onion",
	  pt: "Cebola Agridoce"
	},
	"Mostarda e Mel": {
	  en: "Mustard and Honey",
	  pt: "Mostarda e Mel"
	},
	"Maionese": {
	  en: "Mayonnaise",
	  pt: "Maionese"
	},
	"Parmesão": {
	  en: "Parmesan",
	  pt: "Parmesão"
	},
	"Italiano": {
	  en: "Italian",
	  pt: "Italiano"
	},
	"Apimentado": {
	  en: "Spicy",
	  pt: "Apimentado"
	},
	"Temperos:": {
	  en: "Spices:",
	  pt: "Temperos:"
	},
	"Pimenta": {
	  en: "Chili",
	  pt: "Pimenta"
	},
	"Sal": {
	  en: "Salt",
	  pt: "Sal"
	},
	"Orégano": {
	  en: "Oregano",
	  pt: "Orégano"
	},
	"Pimenta calabresa": {
	  en: "Pepperoni Pepper",
	  pt: "Pimenta Calabresa"
	},
	"Valor do Lanche": {
	  en: "Snack Value",
	  pt: "Valor do Lanche"
	},
	"Lanches Escolhidos": {
	  en: "Selected Snacks",
	  pt: "Lanches Escolhidos"
	},
	"Valor do Pedido": {
	  en: "Value of the Order",
	  pt: "Valor do Pedido"
	},
	"Salvar": {
	  en: "Save",
	  pt: "Salvar"
	},
	"Finalizar": {
	  en: "Finish",
	  pt: "Finalizar"
	},
	"Olá:": {
	  en: "Hello:",
	  pt: "Olá:"
	},
	"O valor total do seu pedido é de": {
	  en: "The total value of your order is",
	  pt: "O valor total do seu pedido é de"
	},
	"Qual a forma de pagamento?": {
	  en: "What is the payment method?",
	  pt: "Qual a forma de pagamento?"
	},
	"Crédito": {
	  en: "Credit",
	  pt: "Crédito"
	},
	"Débito": {
	  en: "Debit",
	  pt: "Débito"
	},
	"Dinheiro": {
	  en: "Money",
	  pt: "Dinheiro"
	},
	"Precisa de troco?": {
	  en: "Need change?",
	  pt: "Precisa de troco?"
	},
	"Sim": {
	  en: "Yes",
	  pt: "Sim"
	},
	"Não": {
	  en: "No",
	  pt: "Não"
	},
	"Seu pedido já está em andamento": {
	  en: "Your order is already in progress",
	  pt: "Seu pedido já está em andamento"
	},
	"Use nossa calculadora caso queira dividir o valor": {
	  en: "Use our calculator if you want to split the value",
	  pt: "Use nossa calculadora caso queira dividir o valor"
	},
	"Quantidade de pessoas": {
	  en: "Amount of people",
	  pt: "Quantidade de pessoas"
	},
	"Valor por pessoa": {
	  en: "Value per person",
	  pt: "Valor por pessoa"
	},
	"Pessoa": {
	  en: "Person",
	  pt: "Pessoa"
	},
	"Valor": {
	  en: "Value",
	  pt: "Valor"
	},
	"%": {
	  en: "%",
	  pt: "%"
	},
	"Faça seu pedido!": {
	  en: "Order your snacks!",
	  pt: "Faça seu pedido!"
	},
	"Carregando": {
	  en: "Loading",
	  pt: "Carregando"
	},
};
  
var translator;
$(function() {
	
	translator = $('body').translate({t: dictionary}); 

});
