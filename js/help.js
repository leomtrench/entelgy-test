

function addHasError(element){
	$(element).parent().addClass('has-error');
	$(element).focus();
}

function removeHasError(element){
	$(element).parent().removeClass('has-error');
}

//Gera as <option>		
function addOption(id, value, text, price){ 
	$('#'+id).attr('disabled',false);
	$('#'+id).attr('data-calculate','calculate');
	$('#'+id).append($('<option>', {
		value: translator.get(value),
		text: translator.get(text),
		price: price
	}));
}

//Gera os <input>	
function addInput(id, value, price, type){ 
	value = translator.get(value);	
	$('#'+id).append(
		$('<label>', {
			class: 'radio-inline'
		}).append(
			$('<input>',{
				id: id,
				value: value,
				name: id,
				type: type,
				price: price,
				'data-calculate':'calculate',
				
			})
		).append(value)
	);
}

//Pega parametros get da URL
function getURLParameter(name) {
  return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search) || [null, ''])[1].replace(/\+/g, '%20')) || null;
}
